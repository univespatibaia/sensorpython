'''
Esse programa faz a verificação do nível d'água e conforme um padrão pré definido, toma decisão
de enviar ou não para API do site.
Caso a coluna d'agua seja maior que o determinado fará fotografia e irá enviar os dados
'''

from classSensorColuna import SColuna
from classCamera import Camera
from MSSQL import MaximoNivel
import time
import requests
import sirene



#urlAPI = "http://localhost:51485/api/APIRaspberry/RecebeDados?"
urlAPI = "https://enchente.azurewebsites.net/api/APIRaspberry/RecebeDados?"
PontoRP = 1

SC = SColuna()
foto = Camera()
sensor = MaximoNivel(PontoRP) #Ponto 1 cadastrado no MSSQL

while True:
    print("")
    time.sleep(1)
    ponto1MaxColuna = sensor.Retorna() #cm de coluna dágua 450 = 4,50m
    print("Ponto1: "  + str(ponto1MaxColuna))
    leituraSensorAtual = SC.Kpa()
    print("Atual.: ", str(leituraSensorAtual))

    if leituraSensorAtual > ponto1MaxColuna:
        #sirene.Sirene()
        print("######## POSSÍVEL ENCHENTE!!!!")
        nomeFoto = foto.Fotografar()

        files = {'files': open(nomeFoto, 'rb')}
        r = requests.post(urlAPI, data={'Ponto':PontoRP,'NivelAtual':leituraSensorAtual}, files=files)
        print(r.text)

        #send foto e dados via API
    else:
        print("nível normal")

